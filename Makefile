.PHONY: test phpunit pyunit
CURRENT_DIRECTORY=$(shell pwd)
CONTAINER_NAME:=kasmeer_bot_tests_container_$(shell uuidgen)
IMAGE_NAME:=kasmeer_bot_tests$(shell uuidgen | tr "[:upper:]" "[:lower:]")

tests: style_check pyunit

install_packages:
	pip install -r requirements.txt

pyunit:
	PYTHONPATH=$(CURRENT_DIRECTORY)/tests python3 -m unittest tests

style_check:
	python3 -B -m pylint --rcfile=tests/pylintrc  commands/ config/ messages/ tests/

bot-run:
	python3 main.py

"""
Module that get the discord token from the environment variable
"""
import os
__token__ = os.environ.get('DISCORD_TOKEN')

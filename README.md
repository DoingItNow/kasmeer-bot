# Kasmeer Bot
This is a discord bot that does the following

* Assign colour roles to guildies
* Subscribe people to different text channels
* Log who has left or entered a voice channel

## How to run
In order to run this bot locally you need the following:
* Python 3.6+

## Install Dependencies
### Windows
Run the following command: `pip install -r requirements.txt`

### Mac/Linux
Run the command: `make install_packages`

## Set bot locally
Get the discord token and save it as the environment variable `DISCORD_TOKEN`

Run `python main.py` or `make bot-run`

## How to authorise bot to join your server
Get the client ID of the bot and paste it into the URL `https://discordapp.com/oauth2/authorize?client_id=CLIENTID&scope=bot&permissions=488111216` where `CLIENTID` is the client_id

## Run the tests
To run unit tests use `python -m unittest tests`

For Mac/Linux use `make tests`

## Run tests in a container
* Create the image first by using `make build_docker_test`
* Run the tests using `make -k make run_docker_test`

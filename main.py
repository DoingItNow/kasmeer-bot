"""
Main module for Kasmeer Bot
"""
from commands import colour_roles
from commands import setup
from commands import purge
from commands import modify_channels
from commands import join_raids_command
from commands import sub_group_role
from commands import public_channels
from commands import team_chat_roles
from commands import join_upoi_command

import discord
from discord.ext import commands

from messages import help_messages
from messages import logging_messages
from messages import private_messages
from messages import role_messages
from messages import message_handler
from messages import fluff_messages

from config import config

bot = commands.Bot(command_prefix='.')
bot.remove_command("help")

CLIENT = discord.Client()


@bot.event
async def on_ready():
    """
        Function that calls client is done preparing the data received from Discord
    """

    print('Logged in as')
    client_array = setup.get_client_details(bot)
    print(client_array[0])
    print('------')
    await bot.change_presence(game=discord.Game(name='1.6.5'))


@bot.event
async def on_message(message):
    """
        Function that is called when someone messages where kasmeer can see
    """
    await private_messages.process_incoming_message(message, bot)
    await bot.process_commands(message)


@bot.event
async def on_voice_state_update(before_user, after_user):
    """
        Function that is called when someone updates their voice state
    """
    # If the member's voice channel is NOT None, he has joined a channel
    if after_user.voice.voice_channel != None:
        await logging_messages.send_user_joined_response(after_user, after_user.voice.voice_channel, bot)

        await public_channels.check_voice_channel_state(after_user, before_user.voice.voice_channel, after_user.voice.voice_channel, bot)

    # If the members voice channel is None, he has
    elif after_user.voice.voice_channel is None:
        users_original_voice_channel = before_user.voice.voice_channel
        await logging_messages.send_user_left_response(after_user, users_original_voice_channel, bot)

        await public_channels.check_voice_channel_state(after_user, before_user.voice.voice_channel, after_user.voice.voice_channel, bot)


@bot.command(pass_context=True)
async def colour(ctx, *, selected_colour: str):
    """
        Function that calls to assign a colour for the user
    """

    message = ctx.message

    role_assigner = colour_roles.ColourRoles()

    # Get the specific variables from the message
    colour_assigned_channel = message.channel
    user = message.author
    user_role_list = message.author.roles

    # Get the selected colour and the current colour
    selected_colour_role_string = role_assigner.get_selected_colour(
        selected_colour)
    selected_colour_role = discord.utils.get(
        message.server.roles, name=selected_colour_role_string)

    current_colour_role_string = role_assigner.get_current_server_role(
        user_role_list)
    current_colour_role = discord.utils.get(
        message.server.roles, name=current_colour_role_string)

    # Get the role response
    role_response = role_assigner.get_assign_role_response(
        colour_assigned_channel, user_role_list, selected_colour_role)
    await role_assigner.assign_colour_role(role_response, user, selected_colour_role, current_colour_role, bot)

    # Send message
    role_message = role_messages.create_role_message(
        role_response, "assign", user, selected_colour_role_string)

    message_handle = message_handler.MessageHandler()
    await message_handle.say_message(role_message, bot)


@bot.command(pass_context=True)
async def uncolour(ctx, *, selected_colour: str):
    """
        Function that calls to remove a colour for the user
    """
    message = ctx.message

    role_assigner = colour_roles.ColourRoles()

    colour_assigned_channel = message.channel
    user = message.author
    user_role_list = message.author.roles

    selected_colour_role_string = role_assigner.get_selected_colour(
        selected_colour)
    selected_colour_role = discord.utils.get(
        message.server.roles, name=selected_colour_role_string)

    # Get the role response
    role_response = role_assigner.get_remove_role_response(
        colour_assigned_channel, user_role_list, selected_colour_role,)

    await role_assigner.remove_colour_role(role_response, user, selected_colour_role, bot)

    # Send message
    role_message = role_messages.create_role_message(
        role_response, "remove", user, selected_colour_role_string)

    message_handle = message_handler.MessageHandler()
    await message_handle.say_message(role_message, bot)


@bot.command(pass_context=True)
async def help(ctx):
    """
        Function that calls to get the help message for the user
    """
    member = ctx.message.author
    message = ctx.message
    await help_messages.send_help_response(message, member, bot)


@bot.command(pass_context=True)
async def help_channel(ctx):
    """
        Function that calls to get the help message for the user
    """
    member = ctx.message.author
    message = ctx.message
    await help_messages.get_help_channel_response(message, member, bot)


@bot.command()
async def list():
    """
        Function that calls to get the list of available colours
    """
    await help_messages.send_list_response(bot)


@bot.command(pass_context=True)
async def purge_msg(ctx, channel: str, number: int):
    """
        Function that calls to get purge N number of messages
    """
    message = ctx.message
    member = ctx.message.author
    await purge.purge_logs(number, member, channel, message, bot)


@bot.command(pass_context=True)
async def join_upoi(ctx):
    """
        Function that calls to join poi discord server
    """
    # Set the context
    message = ctx.message
    await join_upoi_command.message_bot_private_message(message, bot)


@bot.command(pass_context=True)
async def join_raids(ctx):
    """
        Function that calls to join raids in poi
    """
    # Set the context
    message = ctx.message
    await join_raids_command.message_bot_private_message(message, bot)


@bot.command(pass_context=True)
async def sub_group(ctx, *, lfm_type: str):
    """
        Function that calls to subscribe to a particular text channel
    """

    # Set the context
    message = ctx.message
    await sub_group_role.assign_subscribed_role(message, lfm_type, bot)


@bot.command(pass_context=True)
async def create_log_channels(ctx):
    """
        Function that calls to create log channels
    """
    # Set the context
    message = ctx.message
    await modify_channels.modify_logs_text_channel("create", message, bot)


@bot.command(pass_context=True)
async def delete_log_channels(ctx):
    """
        Function that calls to delete log channels
    """
    # Set the context
    message = ctx.message
    await modify_channels.modify_logs_text_channel("delete", message, bot)


@bot.command(pass_context=True)
async def create_team_chat_roles(ctx):
    """
        Function that calls to create team chat roles
    """
    # Set the context
    message = ctx.message
    await team_chat_roles.create_team_roles(bot, message)


@bot.command(pass_context=True)
async def delete_team_chat_roles(ctx):
    """
        Function that calls to delete team chat roles
    """
    # Set the context
    message = ctx.message
    await team_chat_roles.delete_team_roles(bot, message)


@bot.command(pass_context=True)
async def highiqchrono(ctx):
    """
        Function that calls to state who is a high IQ chrono
    """
    # Set the context
    channel = ctx.message.channel
    await fluff_messages.send_high_iq_chrono_response(channel, bot)


bot.run(config.__token__)

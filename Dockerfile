FROM python:3.6.5-slim-jessie

# Add NBN certs into the container
RUN apt-get update 
RUN apt-get install -y make uuid-runtime

WORKDIR /python
COPY Makefile /python
COPY requirements.txt /python
RUN make install_packages

ENTRYPOINT [ "make", "-k", "tests" ]

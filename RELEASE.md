# Kasmeer Bot Release Notes

## 1.6.5
* Added two new colour roles: Light Greena and Stinky (Brown)
* Updated Python Version to 3.6.6

## 1.6.4
* Updated join upoi command with updated text
* Removed join raid temporary for the new changes to raid training

## 1.6.3
* Created team public chat text channels when users join team channels. Users will get assigned a role when joining a voice channel
    * Once there are no users, the channel is deleted
    * ISSUE: Text channels are created without a category. The version of this API does not support creating channels in a category

## 1.6.2
* Cleaned up code with pylint and updated docstrings for every function/class
* Split join_messages into two separate files. Changed `join_group raids` to `join_raids` as there was not need to have `raids` as a key string
* Added Makefile commands to run tests in a container and easier commands to run

## 1.6.1
* Redesigned how colour roles was done. Changed commands to `colour` instead of `role`
* Added Trash Grey and Telegram Blues colours

## 1.6.0
* Added three self assigned roles to subscribe to: raid-lfm, fractal-lfm and anime roles. Each role can be removed if needed
* Renamed assign-colours channel to kasmeers-playground to make the channel be general commands for Kasmeer
* Updated help to be posted in a text channel so all users can see

## 1.5.0
* Added new self allocation for raids
* Refactor importing modules to make it look alot neater

## 1.4.1
* Modified text channel to be in a separate logging text channel
* Created two admin functions that creates and deletes log channels

## 1.4.0
* Created self serviced feature for new people that want to join uPOI Discord Server
    * New people will message the bot which will give them the rules of POI and if they accept, will get the New Poi role and go to the welcome mat 

## 1.3.2
* Added new colours

## 1.3.1
* Changed roles, joined/leave channel messages to be normal text messages instead of embedded messages
* Changed Very Orange to Exotic Orange

## 1.3.0
* Added logging functions for channels
    * For team channels whenever someone joins/leaves the voice channel will get a message in the corrosponding channels
    * General channels will be logged into one channel
* Added a batch message delete function
    * This is for admins only and can only be done in a specific channel. It is a powerful function which can unwilling destroy messages at will. Use this power wisely
* Minor text changes to messages sent via bot
* Minor refactoring of modules

## 1.2.2
* Minor text fixes for .lists and generic help responses

## 1.2.1
* Refactored bot to use `bot.command()` module which removes the need of having a separate module to call specific commands
    * `commands.py` would have custom made code to determine if a command existed or not. There was some extra functions that was needed to get it to work but using `discord.py` ext commands made `commands.py` functions redundant.
* Added another whitelisted channel that can assign or remove roles
* Changed Sexy Pink role name to Jon's Pink

## 1.2.0
* Added help commands for admins and normal users
* Changed role function to remove the previous role it was assigned to

## 1.1.0
* Refactored assignment of colours so that users can assign colours by using the generic name or the exact colour role
* Added embedded colours to the message sent
* added lists and help commands

# 1.0.0 (Initial Release)
* Created bot that has the follow commands
    * role
    * unrole
* Set 5 basic colours to choose from
"""
Module showing the messages when creating or deleting the text channels for logs
"""

async def send_create_channel_response(bot_command):
    """
        Sends a message regarding the creation of log channels
    """

    create_channel_message = "Team Log Channels has been created!"
    await bot_command.say(create_channel_message)

async def send_delete_channel_response(bot_command):
    """
        Sends a message regarding the deletion of log channels
    """

    delete_channel_message = "Team Log Channels has been sent to the mists."
    await bot_command.say(delete_channel_message)

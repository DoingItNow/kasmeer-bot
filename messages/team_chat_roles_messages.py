"""
Module showing the messages when creating or deleting the team chat roles
"""

async def send_created_roles_response(bot_command):
    """
        Sends a message regarding the creation of team chat roles
    """

    create_channel_message = "Team Chat Roles Channels has been created!"
    await bot_command.say(create_channel_message)

async def send_delete_roles_response(bot_command):
    """
        Sends a message regarding the deletion of team chat roles
    """

    delete_channel_message = "Team Chat Roles has been sent to the mists."
    await bot_command.say(delete_channel_message)

"""
Class module to hanlde different sending of messages
"""
class MessageHandler():
    """Class module to hanlde different sending of messages
    """
    def __init__(self):
        """Empty init
        """
        return

    async def say_message(self, message, bot):
        """Send a message in the current text channel

        :param message: A string containing the message
        :param bot: A discord bot
        """
        await bot.say(message)

    async def send_message(self, message, location, bot):
        """Send a message to a particular channel

        :param message: A string containing the message
        :param bot: A discord bot
        :param location: A channel object
        """
        await bot.send_message(location, message)

    async def whisper(self, message, bot):
        """Send a whisper to the bot

        :param message: A string containing the message
        :param bot: A discord bot
        """
        await bot.whisper(message)
    
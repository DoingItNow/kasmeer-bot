"""
Function that creates the role message whcih is sent to the user
"""

def create_role_message(message_type, command_type, user, colour):
    """Creates a message based on the message type

    :param message_type: A string that has the message type
    :param command_type: A string determining if a role is assigned or removed
    :param user: A user object
    :param colour: A string that says the colour name
    :rtype: A string with the message that is going to be sent to the user
    """
    if message_type == "role_nonexistent":
        role_message = "Sorry! Either you don't have permission or the role does not exist. " \
                       "Type in `.list` to check the colour roles"

    elif message_type == "wrong_channel":
        role_message = "What did I tell you? I cannot perform my magic here!\n" \
                        + "Go to the kasmeers-playground channel and do it there.\n\nThanks!\nLove Kas"

    elif message_type == "can_perform_action":
        if command_type == "assign":
            role_message = user.mention + " you now have the **" + colour + "** role."
        else:
            role_message = user.mention + " you have removed the **" + colour + "** role."

    elif message_type == "action_already_done":
        if command_type == "assign":
            role_message = user.mention + " you already have the **" + colour + "** role."
        else:
            role_message = user.mention \
                            + " you don't have the **"  \
                            + colour + "** role in the first place!"
    return role_message

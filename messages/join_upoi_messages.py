"""
Module containing all the join raids and upoi messages
"""
async def already_joined_poi(member, bot):
    """
        Returns the message saying they are already a POI member
    """
    deny_message = "You are already a member of the glorious guild uPOI"
    await bot.send_message(member, deny_message)

async def give_rules(bot):
    """
        Returns the message saying the type in the prompt
    """
    rules_message = "Hello, I am Kasmeer! I will help you get into our guild and give access to chat channels.\n" \
                    "Please read the #rules and make sure you understand them.\n\n" \
                    "Once you agree to them and wish to join our guild, type in `I accept the rules and I will be a good POI`"
    await bot.whisper(rules_message)

async def message_welcome_mat(channel, bot_help_channel, member, bot):
    """
        Returns the message saying they have joined POI
    """
    welcome_message = member.mention + " Welcome to the uPOI Discord Server!\n" \
                        "Make sure to post up your introduction in this channel! Also refer to " + bot_help_channel.mention + \
                        " if you want to get a colour for your discord name."

    success_message = "Welcome to **uPOI**! You now have access to #welcome-mat channel. " \
                      "This where the guild can get to know you a little bit before we toss you into the vast open abyss of home chat.\n" \
                      "Please fill this template and post it into the #welcome-mat channel.\n\n" \
                      "**Name**: What do we call you?\n" \
                      "**Timezone**: What timezone are you in? GMT +10, GMT +8, etc\n" \
                      "**Account name**: Example.xxxx, so we can invite you ingame.\n" \
                      "**Joined via**: How did you find us, or who referred you?\n" \
                      "**Favourite thing to do ingame**: Could be broad, like raid or WvW or specific, " \
                      "like that one jumping puzzle in Metrica Province that ports you all over the map in different stages and is SO painful at the bit with the wind.\n" \
                      "**Mains**: Your favourite classes or characters.\n" \
                      "**Pet preferences**: Are you a cat person or a dog person? Or something else, like lizards?\n" \
                      "**Other games**: Overwatch? Monster Hunter World? Destiny 2? Starbound? The possibilities are endless!\n" \
                      "**Notes**: Anything else you want us to know!\n\n" \
                      "Once that is done, an officer will give you the guildy tag and give you full access to guild channels."

    await bot.send_message(channel, welcome_message)
    await bot.send_message(member, success_message)

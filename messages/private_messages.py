"""
Module which handles when users message the bot via PM
"""

from commands import join_raids_command
from commands import join_upoi_command
from commands import sub_group_role
from messages import join_raids_messages

async def process_incoming_message(message, bot):
    """Process the message to the appropriate functions

    :param message: A string containing the message
    :param bot: A discord bot
    """
    # if the message is private and contains correct content, craete the new roles
    if message.content == "I accept the rules and I will be a good POI" and message.channel.is_private:
        user = message.author
        await join_upoi_command.assign_new_poi_role(user, bot)
    elif message.content == "I am interested in a static team and/or raid trainings!" and message.channel.is_private:
        user = message.author
        await join_raids_messages.give_raid_rules_part_1(user, bot)
    elif message.content == "I understand Kas. Whats next?" and message.channel.is_private:
        user = message.author
        await join_raids_messages.give_raid_rules_part_2(user, bot)
    elif message.content == "I have completed the sheet!" and message.channel.is_private:
        user = message.author
        await join_raids_command.assign_new_raider_role(user, bot)
    elif message.content == "I admit I am a trash weeb. Take me to all the other weebs" and message.channel.is_private:
        user = message.author
        await sub_group_role.assign_anime_role(user, bot)

"""
Module that sends message regarding purging of messages
"""

async def send_deny_purge_response(bot_command):
    """
        Sends a message about denying the command
        to delete logs due to incorrect channel
    """
    wrong_channel_message = 'I am sorry. What was I trying to do? Oh yes, that thing...' \
                    'It\'s rather a secret thing I like to do in private\n\n' \
                    'Please do this in my secret channel #kasmeer-culling-shack'
    await bot_command.whisper(wrong_channel_message)


async def send_purge_completed_response(bot_command, culling_channel, message_deleted_channel, number):
    """
        Sends a message about successfully removing messages
    """
    if number == 1:
        kill_message = 'Wow pathetic? Just one message. Guess it will do.'

    elif number == 0:
        kill_message = 'Nope there is absolutely nothing to kill here. Not a single thing.'
    else:
        kill_message = 'Things have been deleted\n' + \
                        'I have sent ' + str(number) + ' messages from ' + message_deleted_channel.name + ' into the mists.'

    await bot_command.send_message(culling_channel, kill_message)

"""
Module containing all the join raids and upoi messages
"""
async def ask_interested_statics_message(bot):
    """
        Returns the message about interests in raids
    """
    rules_message = "Hi there, looks like you're interested in raids! There's a few things we need you to ask you first. " \
                    "Are you interested in joining a static team, filling for teams or raid trainings?\n\n" \
                    "A static team means that you are going to be on a team which has set times and days you raid in.\n\n" \
                    "If you are just interested in filling for teams, nothing needs to be done! Just look at #raid-lfm which " \
                    "will show you any teams that are needing a fill.\n\n" \
                    "If you are interested a static team or get some raid training type in `I am interested in a static team and/or raid trainings!`\n\n" \
                    "Any questions or issues, ping @Jon#1529 for assistance"

    await bot.whisper(rules_message)

async def give_raid_rules_part_1(member, bot):
    """
        Returns the message saying the type in the prompt
    """
    rules_message = "Sweet you are interested in static teams! \n\n" \
                    "Our guild has a training system in place to help people learn as well as get a new team, but there are a few things " \
                    " we need you to understand first:.\n\n" \
                    "[1]: Even if you're super pro, you may not find a team with us due to no times overlapping or clashing personalities.\n\n" \
                    "[2]: Sometimes raids are not meant for everyone, it's intended to be the top 10% of the game in terms of difficulty. " \
                    "We will try our best to train you, teach you, and explain what we can, but we've met many a troubled player.\n\n" \
                    "[3]: Signing up for raid static team is a long term commitment. Please don't sign up if you're going on holiday in a month, " \
                    "or have changing shifts at work. " \
                    "Newer teams are expected to run twice a week for 2 hours each. " \
                    "Older teams ideally will get better and compress these into a day.\n\n" \
                    "[4]: Raids is also a time investment. In order to get better at raids, " \
                    "we expect you to run raids outside of Guild raid time in order " \
                    "to practice and get better. " \
                    "The team run is not the place for your to learn.\n\n" \
                    "If you're all good on that, type `I understand Kas. Whats next?`"

    # When on_message picks up the message, it sends the message to the user
    await bot.send_message(member, rules_message)

async def give_raid_rules_part_2(member, bot):
    """
        Returns the message saying the type in the prompt
    """
    rules_message = "Great!\n\n" \
                    "All our guild raiders will go through a check to make sure you're up to scratch with a training team we call Team Aurora. " \
                    "Veteran or newbie, everyone is expected to attend some session " \
                    "or multiple sessions to make sure there's no problems down the line. There will be more information in the team chat.\n\n" \
                    "Next please fill out this Google doc, it's best on desktop: " \
                    "https://docs.google.com/spreadsheets/d/1H2-4k7PYQnz34sRSklmUs4KqHzejQ8GXqoHJzHPA1aM/edit?usp=sharing\n" \
                    "Times are in AEST (GMT+10), please convert your times.\n\n" \
                    "Once this is done message me (Kasmeer of course!) saying `I have completed the sheet!` " \
                    "and I'll assign you the Team Aurora role."

    # When on_message picks up the message, it sends the message to the user
    await bot.send_message(member, rules_message)

async def message_team_aurora(channel, member, bot):
    """
        Returns the message saying they have joined POI
    """
    welcome_message = member.mention + " has joined Team Aurora as a new raider for POI!"
    success_message = "Great, you are now a part of Team Aurora! Say hi and socialise with other new raiders."
    await bot.send_message(channel, welcome_message)
    await bot.send_message(member, success_message)

"""
Modules where users join or leave a channel it is shown in the
respective team text channels
"""
import discord

def get_team_channel_with_dashes(team_channel):
    """
        Returns the text channel name with dashes
    """
    split_team_name = team_channel.name.lower().split(" ")
    team_with_dashes = split_team_name[0]

    for index in range(1, len(split_team_name)):
        team_with_dashes = team_with_dashes + "-" + split_team_name[index]
    team_with_dashes = team_with_dashes + "-logs"
    return team_with_dashes

def get_team_log_channel(user, team_channel):
    """
        Returns the team's text channel name
    """
    for channel in user.server.channels:
        if channel.name == team_channel:
            return team_channel
    return None

async def send_user_joined_response(user, channel, bot):
    """
        Sends a message about joining a channel
    """
    team_text_channel_name = get_team_channel_with_dashes(channel)
    team_channel_name = get_team_log_channel(user, team_text_channel_name)

    if team_channel_name is None:
        log_channel = discord.utils.get(user.server.channels, name="logs-channel")
    else:
        log_channel = discord.utils.get(user.server.channels, name=team_channel_name)

    channel_message = user.display_name + " has joined the voice channel " +  "**" + channel.name + "**"
    await bot.send_message(log_channel, channel_message)


async def send_user_left_response(user, channel, bot):
    """
        Sends a message about leaving a channel
    """
    team_text_channel_name = get_team_channel_with_dashes(channel)
    team_channel_name = get_team_log_channel(user, team_text_channel_name)

    if team_channel_name is None:
        log_channel = discord.utils.get(user.server.channels, name="logs-channel")
    else:
        log_channel = discord.utils.get(user.server.channels, name=team_channel_name)

    channel_message = user.display_name + " has left the voice channel " +  "**" + channel.name + "**"
    await bot.send_message(log_channel, channel_message)

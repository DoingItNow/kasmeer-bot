"""
Creates help messages for admins and guildies
"""
import discord


async def send_list_response(bot_command):
    """
        Sends a embedded message about the list of
        colours available for the .colour/.uncolour command
    """
    embedded_message = discord.Embed(colour=0x8D15C4)
    embedded_message.title = ""
    field_name = "Kasmeer Bot Help for Roles"
    field_value = ('Hello! I provide magical mesmer services '
                   'in giving guildies colour roles!\n'
                   'The following colours can be chosen\n\n'
                   '- Blue\n- Green\n- Pink\n- Purple\n- Yellow\n- Orange\n- Red\n'
                   '- Teal\n- Olive\n- Banana\n- Salmon\n- Grey\n- Cyan\n- Brown\n'
                   '- Telegram\n- Light Green\n- Stinky\n\n'
                   'The commands are as follows:\n'
                   '```.colour [colour] - Assigns the colour you want\n'
                   'Example: .colour blue or .colour Blue\n\n'
                   '.uncolour [colour] - removes the role from you. Note: Use only if you have two colour roles assigned\n'
                   'Example .uncolour red or .uncolour Red```\n\n')

    embedded_message.add_field(name=field_name, value=field_value)
    await bot_command.say(embed=embedded_message)


async def send_help_response(message, member, bot_command):
    """
        Sends an embedded message regarding the information
        about the bot itself
    """
    admin_role = discord.utils.get(member.server.roles, name="Admin")

    # Admin and Guildy get different help responses
    if admin_role in member.roles and message.channel.name == "kasmeer-culling-shack":
        embedded_message = discord.Embed(colour=0x8D15C4)
        embedded_message.title = ""

        # Create embedded fields to be added into the embedded message
        admin_fields = get_admin_help_response()

        embedded_message.add_field(name=admin_fields[0], value=admin_fields[1])
        await bot_command.say(embed=embedded_message)


async def get_help_channel_response(message, member, bot_command):
    """
        Sends a message used in the #bot-help channel
    """
    await bot_command.delete_message(message)
    admin_role = discord.utils.get(member.server.roles, name="Admin")
    assign_colours_channel = discord.utils.get(
        member.server.channels, name="kasmeers-playground")
    team_aurora_channel = discord.utils.get(
        member.server.channels, name="team-aurora")
    weeb_channel = discord.utils.get(
        member.server.channels, name="weeb-nation")

    if admin_role in member.roles:
        help_message = "Hello! I am Kasmeer.\n\nI provide many different sort of functions to guildies! I will list them down below:\n\n" \
                       "**Give Colours to your Username **\n" \
                       "I can set different sort of colours to people. " \
                       "Hop on to " + assign_colours_channel.mention + " and type `.list` to see available colours\n" \
                       "Changing colours will automatically remove the previous colour you had before.\n\n" \
                       "If for some reason you have two colour roles, simply type in `.unrole [colour]` to remove that colour\n" \
                       "Want a new colour? It needs to be unique and have a fancy name to be considered. " \
                       "Message Jon if you have an idea of a new colour.\n\n" \
                       "**Add yourself to raid LFM and fractal LFM channels**\n" \
                       "To have access to Raid LFM and Fractal LFM message Kasmeer in " + assign_colours_channel.mention + \
                       " channel with the following command.\n" \
                       "`.sub_group raid-lfm` for Raid LFMs.\n" \
                       "`.sub_group fractal-lfm` for Fractal LFMs.\n" \
                       "Type in the same command to remove the lfm role.\n\n" \
                       "**Are you a weeb? **\n" + \
                       weeb_channel.mention + ": For all things anime and weebs. Needs to be a weeb to join. " \
                       "Message Kasmeer (via PM) with `.sub_group anime`. " \
                       "To remove yourself from the weeb/anime channel, type in the same command.\n\n" \
                       "**Interested in raids?**\n" + \
                       team_aurora_channel.mention + ": To get into raids and static teams in POI. Message Kasmeer with `.join_raids`\n\n" \

    await bot_command.say(help_message)


def get_admin_help_response():
    """
        Returns an array that contains strings of the help response for admins
    """
    embed_field_array = []

    intro_name = "Kasmeer Bot Help for Admins"
    intro_value = ('Hello! I provide magical mesmer services in many things.\n'
                   'As an admin you have the following commands\n'
                   '```.purge_msg [channel_name] [number] - batch deletes messages in a channel. '
                   'Note: Only deletes messages that are 14 days or newer\n'
                   'Example: purge_msg team-kudzu 50\n\n'
                   '.create_log_channels - Creates all the log channels with the permissions\n\n'
                   '.delete_log_channels - Deletes all the logs channels\n\n'
                   '.create_team_chat_roles - Creates team chat roles into the server\n\n'
                   '.delete_team_chat_roles - Deletes team chat roles into the server\n\n'
                   '.help_channel - Posts the help info for kasmeer```\n\n')

    embed_field_array.append(intro_name)
    embed_field_array.append(intro_value)
    return embed_field_array

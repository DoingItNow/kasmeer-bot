"""
Fluff message module
"""
async def send_high_iq_chrono_response(channel, bot):
    """
        Sends a Rick and Morty copypasta about high IQ chrono players
    """
    fluff_message = "To be fair, you have to have a very high IQ to understand Chronomancer. " \
                    "The rotation is extremely subtle, and without a solid grasp of time magics, most of the spells " \
                    "will go over a typical dps player's head. " \
                    "There's also the Chronomancer's continuum split, which is deftly woven into the Chronomancer's lore " \
                    "- his personal philosophy draws heavily from Krytan literature, for instance. " \
                    "The Chronomancer players understand this stuff; they have the intellectual capacity to truly " \
                    "appreciate the depths of time magics, to realize that the Chronomancer is not just a specialisation " \
                    "that you play- it is so much deeper than that. As a consequence people who dislike playing " \
                    "Chronomancer truly ARE idiots- of course they wouldn't appreciate the intricate synergy between the " \
                    "Chronomancer traits, requiring an in-depth understanding of not only the function but the origin  " \
                    "of each and every trait. For instance, the Master tier trait, \"Danger Time\", is a reference " \
                    "in both name and effects to the video game \"Guilty Gear Xrd -SIGN-\". " \
                    "I'm smirking right now just imagining one of those addlepated dps players scratching their heads in " \
                    "confusion as Mike O'Brien's genius unfolds itself on their computer screens. " \
                    "What fools... how I pity them. :joy:\n\n" \
                    "And yes by the way, I DO have a Chronomancer tattoo. And no, you cannot see it. " \
                    "It's for the ladies' eyes only- And even they have to demonstrate that they're within " \
                    "5 toughness points of my own (preferably higher) beforehand."
    await bot.send_message(channel, fluff_message)

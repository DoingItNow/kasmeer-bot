"""
Module that handles messages when users subscribe to different text channels/roles
"""
async def send_role_response(message_type, message, bot_command, lfm_type):
    """
        Sends an embedded message regarding role assignment or removal
    """
    if message_type == "assigned":
        role_message = message.author.mention + " you now have the **" + lfm_type + " ** role."
    elif message_type == "removed":
        role_message = message.author.mention + " you have removed the **" + lfm_type + " **role."
    await bot_command.say(role_message)

async def give_anime_rules(bot):
    """
        Returns the message with the rules of the anime channel
    """
    rules_message = "So you like anime huh? Are you truly a weeb or are you some typical person that " \
                    "say \"I watch One Piece\" and you consider youself an anime fan?\n" \
                    "When joining this channel, you abide to the following rules:\n\n" \
                    "No lewding any anime characters. This is a strictly SFW channel.\n" \
                    "You talk about all the anime things. This includes who is a trash waifu/husbando and who is truly the best girl/boi.\n" \
                    "Anime memes are allowed. But abide to the first rule of no lewds\n\n" \
                    "If that sounds all good, type in `I admit I am a trash weeb. Take me to all the other weebs`" \

    await bot.whisper(rules_message)

async def message_anime_user(member, bot):
    """
        Returns the message saying they have joined the anime channel
    """
    success_message = "お帰りなさいご主人様。(Check Poi Server to see access to #weeb-nation)"
    await bot.send_message(member, success_message)

async def remove_anime_role_message(bot_command, anime_role):
    """
        Sends an embedded message regarding role assignment or removal
    """

    role_message = "You have removed the **" + anime_role + " **role."
    await bot_command.whisper(role_message)

"""
Unit tests for colour roles
"""
import unittest
from commands import colour_roles
from .mocks import discord_mocks

class ConfigColourTests(unittest.TestCase):
    """Class that defines all the test cases
    """
    def test_colour_class_exists(self):
        """Test the class can be instantiated
        """
        expected = True
        actual = hasattr(colour_roles, '__init__')
        self.assertEqual(expected, actual)

    def test_selected_role_lowercase(self):
        """Test selected colour is chosen correctly
           where the colour is lower case
        """
        role_assigner = colour_roles.ColourRoles()

        expected = "Italian Olive"
        selected_colour = role_assigner.get_selected_colour("olive")

        self.assertEqual(expected, selected_colour)

    def test_selected_role_upperrmulti(self):
        """Test selected colour is chosen correctly
           where the colour is upper case for multiple words
        """
        role_assigner = colour_roles.ColourRoles()

        expected = "Hawks HAIRY hue"
        selected_colour = role_assigner.get_selected_colour("Light Green")

        self.assertEqual(expected, selected_colour)

    def test_selected_role_fullname(self):
        """Test selected colour is chosen correctly
           where the colour is full name
        """
        role_assigner = colour_roles.ColourRoles()

        expected = "Trash Grey"
        selected_colour = role_assigner.get_selected_colour("Trash Grey")

        self.assertEqual(expected, selected_colour)

    def test_selected_role_uppercase(self):
        """Test selected colour is chosen correctly
           where the colour is upper case
        """
        role_assigner = colour_roles.ColourRoles()

        expected = "Cool Cyan"
        selected_colour = role_assigner.get_selected_colour("Cyan")

        self.assertEqual(expected, selected_colour)

    def test_selected_role_none(self):
        """Test selected colour is chosen correctly
           where the colour is incorrect
        """
        role_assigner = colour_roles.ColourRoles()

        expected = "None"
        selected_colour = role_assigner.get_selected_colour("Thing")

        self.assertEqual(expected, selected_colour)

    def test_current_server_role(self):
        """Test if the current colour role from a user is returned
        """
        role_list = discord_mocks.setup_mock_role_list()
        role_assigner = colour_roles.ColourRoles()

        expected_colour = "Rare Yellow"
        actual_colour = role_assigner.get_current_server_role(role_list)

        self.assertEqual(expected_colour, actual_colour)

    def test_assign_response_success(self):
        """Test if colour can be assigned
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playground")
        colour_role = discord_mocks.MockRole("Fine Blue")

        role_assigner = colour_roles.ColourRoles()

        expected_response = "can_perform_action"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_assign_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

    def test_assign_role_wrongchannel(self):
        """Test the wrong channel response is returned
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playgrounds")
        colour_role = discord_mocks.MockRole("Fine Blue")

        role_assigner = colour_roles.ColourRoles()

        expected_response = "wrong_channel"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_assign_role_response(mocked_channel, role_list, colour_role)
        self.assertEqual(expected_response, actual_response)

    def test_assign_role_alreadyexists(self):
        """Test the response is correct when role exists
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playground")
        colour_role = role_list[1] # Colour is Rare Yellow

        role_assigner = colour_roles.ColourRoles()

        expected_response = "action_already_done"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_assign_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

    def test_assign_role_nonexistent(self):
        """Test the response is correct when role doesn't exist
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playground")
        colour_role = None

        role_assigner = colour_roles.ColourRoles()

        expected_response = "role_nonexistent"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_assign_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

    def test_remove_role_success(self):
        """Test if colour can be removed
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playground")
        colour_role = role_list[1] # Colour is Rare Yellow

        role_assigner = colour_roles.ColourRoles()

        expected_response = "can_perform_action"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_remove_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

    def test_remove_role_wrongchannel(self):
        """Test the wrong channel response is returned
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playgrounds")
        colour_role = role_list[1] # Colour is Rare Yellow
        role_assigner = colour_roles.ColourRoles()

        expected_response = "wrong_channel"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_remove_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

    def test_remove_role_responsedone(self):
        """Test the response is correct when role is already removed
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playground")
        colour_role = discord_mocks.MockRole("Fine Blue")

        role_assigner = colour_roles.ColourRoles()

        expected_response = "action_already_done"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_remove_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

    def test_remove_role_nonexistent(self):
        """Test the response is correct when role does not exist
        """
        role_list = discord_mocks.setup_mock_role_list()
        mocked_channel = discord_mocks.MockChannel("kasmeers-playground")
        colour_role = None
        role_assigner = colour_roles.ColourRoles()

        expected_response = "role_nonexistent"

        # Return the value which is comes from the coroutine object
        actual_response = role_assigner.get_remove_role_response(mocked_channel, role_list, colour_role)

        self.assertEqual(expected_response, actual_response)

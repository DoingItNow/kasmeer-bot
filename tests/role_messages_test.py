"""
Unit tests for colour roles
"""
import unittest
from messages import role_messages
from .mocks import discord_mocks

class ConfigRoleMessageTests(unittest.TestCase):
    """Class that defines all the test cases
    """
    def test_message_says_nonexist(self):
        """Check the message output is correct
        """
        mocked_user = discord_mocks.MockUser()

        expected_message = "Sorry! Either you don't have permission or the role does not exist. " \
                           "Type in `.list` to check the colour roles"
        actual_message = role_messages.create_role_message("role_nonexistent", "assign", mocked_user, "Fine Blue")

        self.assertEqual(expected_message, actual_message)

    def test_message_says_wrong_channel(self):
        """Check the message output is correct
        """
        mocked_user = discord_mocks.MockUser()

        expected_message = "What did I tell you? I cannot perform my magic here!\n" \
                        + "Go to the kasmeers-playground channel and do it there.\n\nThanks!\nLove Kas"

        actual_message = role_messages.create_role_message("wrong_channel", "assign", mocked_user, "Fine Blue")

        self.assertEqual(expected_message, actual_message)

    def test_message_action_assign(self):
        """Check the message output is correct
        """
        mocked_user = discord_mocks.MockUser()

        expected_message = "@Bot_Name you now have the **Fine Blue** role."

        actual_message = role_messages.create_role_message("can_perform_action", "assign", mocked_user, "Fine Blue")

        self.assertEqual(expected_message, actual_message)

    def test_message_action_remove(self):
        """Check the message output is correct
        """
        mocked_user = discord_mocks.MockUser()

        expected_message = "@Bot_Name you have removed the **Fine Blue** role."

        actual_message = role_messages.create_role_message("can_perform_action", "remove", mocked_user, "Fine Blue")

        self.assertEqual(expected_message, actual_message)

    def test_already_done_assign(self):
        """Check the message output is correct
        """
        mocked_user = discord_mocks.MockUser()

        expected_message = "@Bot_Name you already have the **Fine Blue** role."

        actual_message = role_messages.create_role_message("action_already_done", "assign", mocked_user, "Fine Blue")

        self.assertEqual(expected_message, actual_message)

    def test_already_done_remove(self):
        """Check the message output is correct
        """
        mocked_user = discord_mocks.MockUser()

        expected_message = "@Bot_Name you don't have the **Fine Blue** role in the first place!"

        actual_message = role_messages.create_role_message("action_already_done", "remove", mocked_user, "Fine Blue")

        self.assertEqual(expected_message, actual_message)

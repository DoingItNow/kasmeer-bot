"""Initial Configuration for modules
"""
from .discord_mocks import setup_mock_role_list
from .discord_mocks import MockChannel
from .discord_mocks import MockRole
from .discord_mocks import MockUser

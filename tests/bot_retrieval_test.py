"""
Unit tests for bot server retrieval file
"""
import unittest
from commands import bot_server_retrieval
from .mocks import discord_mocks

class BotServerTests(unittest.TestCase):
    """Class that defines all the test cases
    """
    def test_marjory_bot_server(self):
        """Test to see Marjory User ID is returned
        """
        bot = discord_mocks.MockBot("Marjory")

        expected_result = "116434225233788934"
        actual_server = bot_server_retrieval.get_bots_server(bot)
        actual_result = actual_server.id
        self.assertEqual(expected_result, actual_result)

    def test_kasmmer_bot_server(self):
        """Test to see Kasmeer User ID is returned
        """
        bot = discord_mocks.MockBot("Kasmeer")

        expected_result = "116178177533018119"
        actual_server = bot_server_retrieval.get_bots_server(bot)
        actual_result = actual_server.id
        self.assertEqual(expected_result, actual_result)

    def test_no_bot_server(self):
        """Test to see Random Bot User ID is not returned
        """
        bot = discord_mocks.MockBot("Random bot")

        expected_result = None
        actual_result = bot_server_retrieval.get_bots_server(bot)
        self.assertEqual(expected_result, actual_result)

    def test_member_from_server(self):
        """Test to a member ID is returned based from a server
        """
        user = discord_mocks.MockUser("user2455666", "1345783845")
        server = discord_mocks.MockServerWithUser()

        expected_result = "1345783845"
        actual_member = bot_server_retrieval.get_member_from_pm(user, server)
        actual_result = actual_member.id
        self.assertEqual(expected_result, actual_result)

    def test_member_from_server_none(self):
        """Test to a member is not returned based from a server
        """
        user = discord_mocks.MockUser("user5", "134578ffff3845")
        server = discord_mocks.MockServerWithUser()

        expected_result = None
        actual_result = bot_server_retrieval.get_member_from_pm(user, server)
        self.assertEqual(expected_result, actual_result)

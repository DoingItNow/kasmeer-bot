"""Initial Configuration for modules
"""
from .colour_roles_test import ConfigColourTests
from .role_messages_test import ConfigRoleMessageTests
from .setup_test import SetupTests
from .bot_retrieval_test import BotServerTests
from .logging_messages_test import TeamLogTests
from .public_channel_test import PublicChannelTests

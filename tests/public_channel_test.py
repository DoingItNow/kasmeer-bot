"""
Unit tests for public channels
"""
import unittest
from commands import public_channels
from .mocks import discord_mocks


class PublicChannelTests(unittest.TestCase):
    """Class that defines all the test cases
    """
    def test_get_voice_channel(self):
        """Test the class can be instantiated
        """
        expected = discord_mocks.MockChannel("test_channel")
        voice_member = discord_mocks.MockUser("a_user", "123445556")
        actual = public_channels.get_voice_channel(voice_member, expected)
        self.assertEqual(expected.name, actual.name)

    def test_getting_team_role(self):
        """Test to see team chat role is outputted based from team channel
        """
        voice_channel = "Team Kudzu"

        expected_result = "team-kudzu-chat"
        actual_result = public_channels.get_team_role_with_dashes(voice_channel)
        self.assertEqual(expected_result, actual_result)

    def test_text_channel_longname(self):
        """Test to see a long team chat role is outputted based from team channel
        """
        voice_channel = "Team Kudzu Best Team"

        expected_result = "team-kudzu-best-team-chat"
        actual_result = public_channels.get_team_role_with_dashes(voice_channel)
        self.assertEqual(expected_result, actual_result)

    def test_text_channel_shortname(self):
        """Test to see a short team chat role is outputted based from team channel
        """
        voice_channel = "Team"

        expected_result = "team-chat"
        actual_result = public_channels.get_team_role_with_dashes(voice_channel)
        self.assertEqual(expected_result, actual_result)

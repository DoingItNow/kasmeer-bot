"""
Unit tests for finding team log channels in logging_messages file
"""
import unittest
from messages import logging_messages
from .mocks import discord_mocks

class TeamLogTests(unittest.TestCase):
    """Class that defines all the test cases
    """
    def test_team_channel_name(self):
        """Test to see team channel is outputted based from user
        """
        user = discord_mocks.MockUser()
        text_channel = "team-kudzu"

        expected_result = "team-kudzu"
        actual_result = logging_messages.get_team_log_channel(user, text_channel)
        self.assertEqual(expected_result, actual_result)

    def test_team_channel_name_none(self):
        """Test to see team channel is not outputted based from user
        """
        user = discord_mocks.MockUser()
        text_channel = "team-kudu"

        expected_result = None
        actual_result = logging_messages.get_team_log_channel(user, text_channel)
        self.assertEqual(expected_result, actual_result)


    def test_team_channel_long_name(self):
        """Test to see a long team channel name is outputted based from user
        """
        user = discord_mocks.MockUser()
        text_channel = "team-ad-bleak-steak"

        expected_result = "team-ad-bleak-steak"
        actual_result = logging_messages.get_team_log_channel(user, text_channel)
        self.assertEqual(expected_result, actual_result)


    def test_getting_text_channel(self):
        """Test to see team channel logs is outputted based from team channel
        """
        voice_channel = discord_mocks.MockChannel("Team Kudzu")

        expected_result = "team-kudzu-logs"
        actual_result = logging_messages.get_team_channel_with_dashes(voice_channel)
        self.assertEqual(expected_result, actual_result)

    def test_text_channel_longname(self):
        """Test to see a long team channel name is outputted based from team channel
        """
        voice_channel = discord_mocks.MockChannel("Team Ad Bleak steak")

        expected_result = "team-ad-bleak-steak-logs"
        actual_result = logging_messages.get_team_channel_with_dashes(voice_channel)
        self.assertEqual(expected_result, actual_result)

    def test_text_channel_shortname(self):
        """Test to see a short team channel name is outputted based from team channel
        """
        voice_channel = discord_mocks.MockChannel("Team")

        expected_result = "team-logs"
        actual_result = logging_messages.get_team_channel_with_dashes(voice_channel)
        self.assertEqual(expected_result, actual_result)

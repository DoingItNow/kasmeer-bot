"""
Module for self service to join POI discord and raids
"""
from commands import bot_server_retrieval
import discord
from messages import join_upoi_messages

async def message_bot_private_message(message, bot):
    """
        Calls the function to send user a private message when join_poi is sent
    """
    # If the message that was sent is from a text channel. delete it
    if not message.channel.is_private:
        await bot.delete_message(message)
    await join_upoi_messages.give_rules(bot)

async def assign_new_poi_role(user, bot):
    """
        Assigned them the new poi role if they entered the prompt correctly
        Denies if they are already a new poi or guildy
    """
    bot_server = bot_server_retrieval.get_bots_server(bot)
    server_member = bot_server_retrieval.get_member_from_pm(user, bot_server)
    member_role_list = server_member.roles

    new_poi_role = discord.utils.get(bot_server.roles, name="New Poi")
    guildy_role = discord.utils.get(bot_server.roles, name="Guildy")
    welcome_channel = discord.utils.get(bot_server.channels, name="welcome-mat")
    bot_help_channel = discord.utils.get(bot_server.channels, name="bot-help")

    # If the user does not have new poi role and (or) doesn't have the guildy role
    # Assign the role to the user
    if not new_poi_role in member_role_list and not guildy_role in member_role_list:
        await bot.add_roles(server_member, new_poi_role)
        await join_upoi_messages.message_welcome_mat(welcome_channel, bot_help_channel, server_member, bot)
    else:
        await join_upoi_messages.already_joined_poi(server_member, bot)

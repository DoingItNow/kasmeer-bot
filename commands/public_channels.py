"""
Module to create or delete text channels when people join voice channels
"""
import discord

def create_team_voice_dictionary():
    """
        returns a dictionary containing team names and channel names
    """
    team_voice_dic = {
        'Team Ad Infinitum' : 'team-ad-infinitum-public-chat',
        'Team Ascension' : 'team-ascension-public-chat',
        'Team Dreamer' : 'team-dreamer-public-chat',
        'Team Eureka' : 'team-eureka-public-chat',
        'Team Flameseeker' : 'team-flameseeker-public-chat',
        'Team Moot' : 'team-moot-public-chat',
        'Team Kudzu' : 'team-kudzu-public-chat',
        'Team Nevermore': 'team-nevermore-public-chat',
        'Team Incinerator' : 'team-incinerator-public-chat',
        'Team Sharur' : 'team-sharur-public-chat',
        'Team Sunrise' : 'team-sunrise-public-chat',
        'Team Twilight' : 'team-twilight-public-chat'
    }
    return team_voice_dic

def get_team_role_with_dashes(team_channel_name):
    """
        Returns the team channel role name with dashes
    """
    split_team_name = team_channel_name.lower().split(" ")
    team_with_dashes = split_team_name[0]

    for index in range(1, len(split_team_name)):
        team_with_dashes = team_with_dashes + "-" + split_team_name[index]
    team_with_dashes = team_with_dashes + "-chat"
    return team_with_dashes

def get_voice_channel(voice_member, voice_channel):
    """Get the voice channel object from the member

    :param voice_member: A member object
    :param channel_name: A string that is the name of the channel
    """
    server_channel_list = voice_member.server.channels
    for selected_channel in server_channel_list:
        if selected_channel.name == voice_channel.name:
            return selected_channel
    return None

async def create_text_channels(voice_member, team_role, team_chat_channel, team_chat_channel_name, bot):
    """Create the text channel for team public chats

    :param voice_member: A member object
    :param team_role: A role object which is the name of the team chat role
    :param team_chat_channel: A channel object which is a team's voice channel
    :param team_chat_channel_name: A string which is the name of the team public text channel
    """
    if team_chat_channel is None:
        everyone_perms = discord.PermissionOverwrite(read_messages=False, send_messages=False)
        kasmeer_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True, manage_channel=True, read_message_history=True)
        team_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True, manage_channel=False, manage_message=False)

        kasmeer_role = discord.utils.get(voice_member.server.roles, name=bot.user.name)

        everyone = discord.ChannelPermissions(target=voice_member.server.default_role, overwrite=everyone_perms)
        kasmeer_perm = discord.ChannelPermissions(target=kasmeer_role, overwrite=kasmeer_perms)
        team_perms = discord.ChannelPermissions(target=team_role, overwrite=team_perms)
        await bot.create_channel(voice_member.server, team_chat_channel_name, everyone, kasmeer_perm, team_perms)

async def update_before_channel(voice_member, before_channel, team_voice_text_dictionary, bot):
    """Updates the text channel before the user changed their voice state

    :param voice_member: A member object
    :param before_channel: A channel object prior to the user changing the voice state
    :param team_voice_text_dictionary: A dictionary containing the key (Team Voice channels)
     and the value (team public chat roles)
    """
    if before_channel is not None:
        if before_channel.name in team_voice_text_dictionary:
            team_chat_role_name = get_team_role_with_dashes(before_channel.name)
            team_chat_channel_name = team_voice_text_dictionary[before_channel.name]

            team_role = discord.utils.get(voice_member.server.roles, name=team_chat_role_name)
            team_chat_channel = discord.utils.get(voice_member.server.channels, name=team_chat_channel_name)

            # If the voice channel is not empty, remove the role from the previous channel the user was on
            if before_channel.voice_members:
                await bot.remove_roles(voice_member, team_role)

            # If the voice channel you left have no members, delete the channel and remove the role
            elif not before_channel.voice_members:
                await bot.delete_channel(team_chat_channel)
                await bot.remove_roles(voice_member, team_role)

async def update_after_channel(voice_member, after_channel, team_voice_text_dictionary, bot):
    """Updates the text channel aafter the user changed their voice state

    :param voice_member: A member object
    :param after_channel: A channel object after the user changed the voice state
    :param team_voice_text_dictionary: A dictionary containing the key (Team Voice channels)
     and the value (team public chat roles)
    """
    if after_channel is not None:
        if after_channel.name in team_voice_text_dictionary:
            team_chat_role_name = get_team_role_with_dashes(after_channel.name)
            team_chat_channel_name = team_voice_text_dictionary[after_channel.name]

            team_role = discord.utils.get(voice_member.server.roles, name=team_chat_role_name)
            team_chat_channel = discord.utils.get(voice_member.server.channels, name=team_chat_channel_name)

            # If the voice channel is not empty, create whatever necessary
            if after_channel.voice_members:
                await create_text_channels(voice_member, team_role, team_chat_channel, team_chat_channel_name, bot)

                # If the user does not have this role already and has not left the channel
                if team_role not in voice_member.roles and voice_member.voice_channel is not None:
                    voice_member_role_list = voice_member.roles

                    # Big Kludge: Find a better way of removing the previous channel role
                    for role in voice_member_role_list:
                        if "-chat" in role.name:
                            voice_member_role_list.remove(role)

                    voice_member_role_list.append(team_role)
                    await bot.replace_roles(voice_member, *voice_member_role_list)

                # If the user has left the channel
                elif voice_member.voice_channel is None:
                    await bot.remove_roles(voice_member, team_role)

async def check_voice_channel_state(voice_member, before_channel, after_channel, bot):
    """Checks the channel states before and after the user voice state change

    :param voice_member: A member object
    :param voice_channel_list: A list containing the voice channels that the user was in
     before and after there was a voice change
    """
    team_voice_text_dictionary = create_team_voice_dictionary()
    await update_before_channel(voice_member, before_channel, team_voice_text_dictionary, bot)
    await update_after_channel(voice_member, after_channel, team_voice_text_dictionary, bot)

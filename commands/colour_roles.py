"""
Class module that assigns the colour roles for guildies
"""
class ColourRoles():
    """
    Class module that assigns the colour roles for guildies
    """
    def __init__(self):
        """Initial values when a ColourRoles class is created.

        :param server_colour_role_list: A list containing the actual server colour names
        :param colour_convert_dic: A dictionary containing colour keys and actual server colour names

        """
        self.server_colour_role_list = [
            "Fine Blue", "Masterwork Green", "Jon's Pink",
            "Rare Yellow", "Exotic Orange", "Legendary Purple",
            "Deadly Red", "Tame Teal", "Italian Olive", "Ripe Banana",
            "Soft Salmon", "Trash Grey", "Cool Cyan", "Try Hard Poo Poo",
            "Telegram Blues", "STINKY", "Hawks HAIRY hue"
        ]

        self.colour_convert_dic = {
            'blue': 'Fine Blue',
            'green': 'Masterwork Green',
            'pink': 'Jon\'s Pink',
            'yellow': 'Rare Yellow',
            'orange': 'Exotic Orange',
            'purple': 'Legendary Purple',
            'red': 'Deadly Red',
            'teal': 'Tame Teal',
            'olive': 'Italian Olive',
            'banana': 'Ripe Banana',
            'salmon': 'Soft Salmon',
            'grey': 'Trash Grey',
            'cyan': 'Cool Cyan',
            'brown': 'Try Hard Poo Poo',
            'telegram': 'Telegram Blues',
            'stinky' : 'STINKY',
            'light green' : 'Hawks HAIRY hue'
        }

    def get_selected_colour(self, colour):
        """Gets a selected colour from the initial colour lists

        :param colour: A string with the name of the colour
        :rtype: A string that has the same name as the colour on the Discord server
        """
        if colour.lower() in  self.colour_convert_dic:
            server_colour = self.colour_convert_dic[colour.lower()]

        elif colour in self.server_colour_role_list:
            server_colour = colour

        else:
            server_colour = "None"
        return server_colour

    def get_current_server_role(self, user_role_list):
        """
        Loops thru each colour in the colour list and
        checks each colour to see if it is in the list of roles
        that the user has.

        :param colour: A list containing user role objects
        :rtype: A role object that is the current colour the user has
        """
        for colour in self.server_colour_role_list:
            for user_role in user_role_list:
                if colour == user_role.name:
                    return colour
        return None

    @staticmethod
    def get_assign_role_response(channel, user_role_list, colour_role):
        """Determines the response of assigning a role

        :param channel: A channel object
        :param user_role_list: A list containing user role objects
        :param colour_role: A role object that is the colour role
        :rtype: A string that determines the next task
        """
        if channel.name == "kasmeer-colours" or channel.name == "kasmeers-playground":
            if colour_role is not None:
                if not colour_role in user_role_list:
                    return "can_perform_action"
                return "action_already_done"
            return "role_nonexistent"
        return "wrong_channel"

    @staticmethod
    def get_remove_role_response(channel, user_role_list, colour_role):
        """Determines the response of removing a role

        :param channel: A channel object
        :param user_role_list: A list containing user role objects
        :param colour_role: A role object that is the colour role
        :rtype: A string that determines the next task
        """
        if channel.name == "kasmeer-colours" or channel.name == "kasmeers-playground":
            if colour_role is not None:
                if colour_role in user_role_list:
                    return "can_perform_action"
                return "action_already_done"
            return "role_nonexistent"
        return "wrong_channel"

    @staticmethod
    async def assign_colour_role(role_response, user, colour_role, current_colour_role, bot):
        """Assigns the colour to the user

        :param role_response: A string containing the response of assigning a role
        :param user: A discord user object
        :param colour_role: A role object that is the colour role
        :param current_colour_string: A string with the name of the current colour role the user has
        :param current_colour_role: A role object that is the current colour the user has
        :param bot: A discord bot object
        """
        if role_response == "can_perform_action":
            user_role_list = user.roles

            # If there is a current colour on the user, remove it from the list
            if current_colour_role is not None:
                user_role_list.remove(current_colour_role)
            user_role_list.append(colour_role)

            # Replace roles will use the user role list to replace old roles
            await bot.replace_roles(user, *user_role_list)

    async def remove_colour_role(self, role_response, user, colour_role, bot):
        """Removes the colour to the user

        :param role_response: A string containing the response of assigning a role
        :param user: A discord user object
        :param colour_role: A role object that is the colour role
        :param bot: A discord bot object
        """
        if role_response == "can_perform_action":
            await bot.remove_roles(user, colour_role)

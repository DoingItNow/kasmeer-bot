"""
Module that gets the server the bot lives and where the user exist in a particular server
"""
def get_bots_server(bot):
    """
        Get the bot's server object
        Kasmeer targets uPOI
        Marjory targets Jon's Server
    """
    bot_name = bot.user.name
    server_list = bot.servers

    # Hardcore server IDs are not ideal
    if bot_name == "Marjory":
        bot_server_id = "116434225233788934"
    elif bot_name == "Kasmeer":
        bot_server_id = "116178177533018119"
    else:
        return None

    for server in server_list:
        if server.id == bot_server_id:
            return server
    return None

def get_member_from_pm(user, server):
    """
        Gets the user object from the private message
        assuming that they are in the server
    """
    main_user_id = user.id
    server_member_list = server.members

    for member in server_member_list:
        if member.id == main_user_id:
            return member
    return None

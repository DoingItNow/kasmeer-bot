"""
Module to purge messages from any text channel
"""
import discord
from messages import purge_messages

async def purge_logs(number, member, channel, message, bot):
    """
        Purges and number of logs from a channel
    """
    admin_role = discord.utils.get(member.server.roles, name="Admin")
    message_to_delete_channel = discord.utils.get(member.server.channels, name=channel)
    channel_to_send = discord.utils.get(member.server.channels, name="kasmeer-culling-shack")

    if admin_role in member.roles:
        message_list = []

        # if it is in the wrong channel, delete message and PM user
        if message.channel.name == "kasmeer-culling-shack":

            # Limit the number of deletions to 100
            if number >= 1 and number <= 100:
                async for deleted_message in bot.logs_from(message_to_delete_channel, limit=number):
                    message_list.append(deleted_message)

                # If there is only one message to delete call a different function
                if len(message_list) > 1:
                    await bot.delete_messages(message_list)
                elif len(message_list) == 1:
                    await bot.delete_message(message_list[0])
                await purge_messages.send_purge_completed_response(bot, channel_to_send, message_to_delete_channel, len(message_list))
        else:
            await bot.delete_message(message)
            await purge_messages.send_deny_purge_response(bot)

"""
Module to subscribe to different text channels using roles
"""
from commands import bot_server_retrieval
import discord
from messages import sub_group_messages

async def assign_subscribed_role(message, group_type, bot):
    """
        Calls the function to add/remove lfm roles to users
    """
    # For sub_group
    # If the message that was sent is from a text channel. delete it
    if not message.channel.is_private and group_type == "anime":
        await bot.delete_message(message)

    if group_type == "anime":
        bot_server = bot_server_retrieval.get_bots_server(bot)
        server_member = bot_server_retrieval.get_member_from_pm(message.author, bot_server)
        member_role_list = server_member.roles

        anime_role = discord.utils.get(bot_server.roles, name="Anime")
        if not anime_role in member_role_list:
            await sub_group_messages.give_anime_rules(bot)
        else:

            await bot.remove_roles(server_member, anime_role)
            await sub_group_messages.remove_anime_role_message(bot, group_type)

    elif group_type == "raid-lfm" or group_type == "fractal-lfm":
        lfm_role = discord.utils.get(message.server.roles, name=group_type)
        user_roles_list = message.author.roles

        if not lfm_role in user_roles_list:
            await bot.add_roles(message.author, lfm_role)
            await sub_group_messages.send_role_response("assigned", message, bot, group_type)
        else:
            await bot.remove_roles(message.author, lfm_role)
            await sub_group_messages.send_role_response("removed", message, bot, group_type)

async def assign_anime_role(user, bot):
    """
        Assigned them the new poi role if they entered the prompt correctly
        Denies if they are already a new poi or guildy
    """
    bot_server = bot_server_retrieval.get_bots_server(bot)
    server_member = bot_server_retrieval.get_member_from_pm(user, bot_server)
    member_role_list = server_member.roles

    anime_role = discord.utils.get(bot_server.roles, name="Anime")

    # If the user does not have new poi role and (or) doesn't have the guildy role
    # Assign the role to the user
    if not anime_role in member_role_list:
        await bot.add_roles(server_member, anime_role)
        await sub_group_messages.message_anime_user(server_member, bot)

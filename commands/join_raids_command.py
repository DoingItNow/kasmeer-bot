"""
Module for self service to join POI raids
"""
from commands import bot_server_retrieval
import discord
from messages import join_raids_messages

async def message_bot_private_message(message, bot):
    """
        Calls the function to send user a private message when join_raids is sent
    """
    # If the message that was sent is from a text channel. delete it
    if not message.channel.is_private:
        await bot.delete_message(message)
    await join_raids_messages.ask_interested_statics_message(bot)

async def assign_new_raider_role(user, bot):
    """
        Assigned them the new poi role if they entered the prompt correctly
        Denies if they are already a new poi or guildy
    """
    bot_server = bot_server_retrieval.get_bots_server(bot)
    server_member = bot_server_retrieval.get_member_from_pm(user, bot_server)
    member_role_list = server_member.roles

    new_raider_role = discord.utils.get(bot_server.roles, name="Team Aurora")
    team_aurora_channel = discord.utils.get(bot_server.channels, name="team-aurora")

    # If the user does not have new poi role and (or) doesn't have the guildy role
    # Assign the role to the user
    if not new_raider_role in member_role_list:
        await bot.add_roles(server_member, new_raider_role)
        await join_raids_messages.message_team_aurora(team_aurora_channel, server_member, bot)

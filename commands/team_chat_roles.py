"""
Module to automate creating for team chat roles for public text channels
"""
import discord
from messages import team_chat_roles_messages


def create_team_role_dictionary():
    """
        returns a dictionary containing team names and channel names
    """
    team_role_dic = [
        'team-ad-infinitum-chat', 'team-ascension-chat', 'team-dreamer-chat', 'team-eureka-chat',
        'team-flameseeker-chat', 'team-moot-chat', 'team-kudzu-chat', 'team-nevermore-chat',
        'team-incinerator-chat', 'team-sharur-chat', 'team-sunrise-chat', 'team-twilight-chat'
    ]
    return team_role_dic


async def create_team_roles(bot, message):
    """Create the team chat roles

    :param message: A message object
    """
    team_role_dictionary = create_team_role_dictionary()
    server = message.server
    member = message.author
    current_channel = message.channel.name

    admin_role = discord.utils.get(server.roles, name="Admin")
    if admin_role in member.roles and current_channel == "kasmeer-culling-shack":
        for role_name in team_role_dictionary:
            if discord.utils.get(server.roles, name=role_name) is None:
                await bot.create_role(server, name=role_name)
        await team_chat_roles_messages.send_created_roles_response(bot)


async def delete_team_roles(bot, message):
    """Delete the team chat roles

    :param message: A message object
    """
    team_role_dictionary = create_team_role_dictionary()
    server = message.server
    member = message.author
    current_channel = message.channel.name
    admin_role = discord.utils.get(server.roles, name="Admin")

    if admin_role in member.roles and current_channel == "kasmeer-culling-shack":
        for role_name in team_role_dictionary:
            deleted_role = discord.utils.get(server.roles, name=role_name)
            await bot.delete_role(server, deleted_role)
        await team_chat_roles_messages.send_delete_roles_response(bot)

"""
Module to create and delete team log channels
"""
import discord
from messages import modify_channels_messages


def create_team_dictionary():
    """
        returns a dictionary containing team names and channel names
    """
    team_dic = {
        'Ad Infinitum': 'team-ad-infinitum-logs',
        'Ascension': 'team-ascension-logs',
        'Dreamer': 'team-dreamer-logs',
        'Flameseeker': 'team-flameseeker-logs',
        'Moot': 'team-moot-logs',
        'Kudzu': 'team-kudzu-logs',
        'Nevermore': 'team-nevermore-logs',
        'Incinerator': 'team-incinerator-logs',
        'Sharur': 'team-sharur-logs',
        'Sunrise': 'team-sunrise-logs',
        'Twilight': 'team-twilight-logs'
    }
    return team_dic


async def modify_logs_text_channel(selection, message, bot):
    """
        Get the selection from the user's message to determine to create or
        delete team log channels
    """
    team_dic = create_team_dictionary()
    if selection == "create":
        for team in team_dic:
            await create_logs_text_channel(bot, message, team_dic[team], team)
        await modify_channels_messages.send_create_channel_response(bot)

    elif selection == "delete":
        for team in team_dic:
            available_channel = discord.utils.get(
                message.server.channels, name=team_dic[team])

            if available_channel != None:
                await delete_logs_text_channel(bot, message, team_dic[team])
        await modify_channels_messages.send_delete_channel_response(bot)


async def create_logs_text_channel(bot, message, name, team_name):
    """
        creates a team text channel with specific permissions per role
    """
    server = message.server
    member = message.author
    current_channel = message.channel.name

    admin_role = discord.utils.get(server.roles, name="Admin")

    if admin_role in member.roles and current_channel == "kasmeer-culling-shack":
        everyone_perms = discord.PermissionOverwrite(
            read_messages=False, send_messages=False)
        kasmeer_perms = discord.PermissionOverwrite(
            read_messages=True, send_messages=True, manage_channel=True, read_message_history=True)
        team_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True, manage_channel=False,
                                                 read_message_history=True, manage_message=False)

        kasmeer_role = discord.utils.get(server.roles, name=bot.user.name)
        team_role = discord.utils.get(server.roles, name=team_name)

        everyone = discord.ChannelPermissions(
            target=server.default_role, overwrite=everyone_perms)
        kasmeer_perm = discord.ChannelPermissions(
            target=kasmeer_role, overwrite=kasmeer_perms)
        team_perms = discord.ChannelPermissions(
            target=team_role, overwrite=team_perms)
        await bot.create_channel(server, name, everyone, kasmeer_perm, team_perms)


async def delete_logs_text_channel(bot, message, team_name):
    """
        deletes team text channels
    """
    server = message.server
    member = message.author
    current_channel = message.channel.name

    admin_role = discord.utils.get(server.roles, name="Admin")

    if admin_role in member.roles and current_channel == "kasmeer-culling-shack":
        channel_to_delete = discord.utils.get(server.channels, name=team_name)
        await bot.delete_channel(channel_to_delete)
